 Una empresa de comercialización de alimentos buscando redefinir su catálogo con información sobre los productos que más se
consumen de acuerdo a los datos sobre gastos del hogar publicados en el siguiente informe
de datos abiertos del Inegi
https://datos.gob.mx/busca/dataset/encuesta-nacional-de-ingresos-y-gastos-de-los-hogare
s-enigh1
Para definir el nuevo catálogo es importante tomar en cuenta únicamente los productos
que fueron adquiridos en los siguientes lugares de interés: Mercado, Tianguis o mercado
sobre ruedas, tiendas de abarrotes, supermercados y tiendas de conveniencia.
El equipo de catálogo espera tener esta información disponible en infinite, nuestra
plataforma de visualización que utiliza metabase como motor. Para ello debes realizar lo
siguiente:
1. Obtener los datos y familiarizarte con ellos ( únicamente es necesario tomar en
cuenta los datos relativos a gastos en el hogar)
2. Utilizando python (o el lenguaje de programación de tu preferencia*) procesar los
datos necesarios para identificar el producto adquirido, el lugar de compra, el tipo
de gasto, la frecuencia de compra, la fecha de adquisición y la forma de pago.
3. Dado que hay columnas solicitadas que no tienen información y con fines del
ejercicio, añadir un valor random dentro de las opciones disponibles para la
columna.
4. Dentro del script agregar código que permita responder alguna de las siguientes
preguntas:
- ¿Cuáles son los productos más adquiridos en los lugares de interés para Jüsto de
acuerdo con la fuente de datos?
- ¿Cuál es el lugar de interés donde las personas adquieren más productos?
5. Dar formato a las columnas que contengan fecha para tener la información lista
para insertar en una base de datos.
6. Proponer el modelo Entidad relación de la tabla que almacenará esta información
dentro de infinite tomando en cuenta que la info debe vivir en una base de datos
relacional MySQL 5.7.
7. Definir el DDL de al menos una de las tablas que se crearán a partir de los datos.
(Puede solo crearse una tabla dependiendo del candidato y su propuesta de
solución)
8. Explicar cómo cambiaría tu solución si tuvieras que trabajar con un archivo de 20GB
o más
9. Opcional: agregar algún tipo de análisis que te parezca interesante en los datos

Entregables:
●

Código en python

●

Diagrama entidad relación

●

DDL de al menos una de las tablas propuestas

●