CREATE TABLE registro_ventas (
    rg_id int NOT NULL,
    tipo_gasto varchar(255) NOT NULL,
    clave varchar(255) NOT NULL,
    forma_pag int NOT NULL,
    lugar_comp int NOT NULL,
    frecuencia int NOT NULL,
    fecha_adqu DATE NOT NULL,
    PRIMARY KEY (rg_id),
    FOREIGN KEY (tipo_gasto) REFERENCES catalogo_tipo_gasto(tipo_gasto),
    FOREIGN KEY (clave) REFERENCES catalogo_gasto(gasto),
    FOREIGN KEY (forma_pag) REFERENCES catalogo_pago(forma_pag),
    FOREIGN KEY (lugar_comp) REFERENCES catalogo_lugar(lugar_comp),
    FOREIGN KEY (frecuencia) REFERENCES catalogo_frecuencia(fercuencia)
);

CREATE TABLE catalogo_lugar (
    lugar_comp int NOT NULL,
    descripcion varchar(255) NOT NULL,
    PRIMARY KEY (lugar_comp)
);

INSERT INTO registro_ventas (rg_id,tipo_gasto,clave,forma_pag,lugar_comp,frecuencia,fecha_adqu) VALUES (0,"G1","C004",1,10,6,'2018-02-01');
INSERT INTO catalogo_lugar (lugar_comp,descripcion) VALUES (10,"tiendas de conveniencia");
SELECT * FROM registro_ventas INNER JOIN catalogo_lugar ON registro_ventas.lugar_comp = catalogo_lugar.lugar_comp;