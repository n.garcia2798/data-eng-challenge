import pandas as pd
import random
from datetime import datetime
import matplotlib.pyplot as plt
import locale
locale.setlocale(locale.LC_ALL,'es_MX.UTF-8')


def filtrar_df(df, lista_filtro):
    filtro = df.lugar_comp.isin(lista_filtro)
    #regresamos un df nuevo para no tener conflictos con slices 
    return pd.DataFrame(df[filtro])

def formato_fecha(fecha):
    fecha = datetime.strptime(fecha, '%B de %Y')
    return fecha.strftime("\'%Y-%m-%d\'")


#datos predefinidos
lugares_de_interes = [1, 2, 4, 10]
columnas_relevantes = ["tipo_gasto", "clave", "forma_pag1", "lugar_comp", "frecuencia", "fecha_adqu"]

#archivo principal
archivo = pd.read_csv("/home/jgarcia/Documents/csvs/conjunto_de_datos_gastoshogar_enigh_2018_ns/conjunto_de_datos/conjunto_de_datos_gastoshogar_enigh_2018_ns.csv",usecols=columnas_relevantes)
#catalogos
catalogo_productos = pd.read_csv("/home/jgarcia/Documents/csvs/conjunto_de_datos_gastoshogar_enigh_2018_ns/catalogos/gastos.csv",encoding='cp1252')
catalogo_lugar = pd.read_csv("/home/jgarcia/Documents/csvs/conjunto_de_datos_gastoshogar_enigh_2018_ns/catalogos/lugar_comp.csv",encoding='cp1252')
catalogo_fecha = pd.read_csv("/home/jgarcia/Documents/csvs/conjunto_de_datos_gastoshogar_enigh_2018_ns/catalogos/fecha_adqu.csv",encoding='cp1252')
catalogo_frecuencia = pd.read_csv("/home/jgarcia/Documents/csvs/conjunto_de_datos_gastoshogar_enigh_2018_ns/catalogos/frecuencia.csv",encoding='cp1252')

#filtrar solo los productos comprados en lugares de interes
datos_compras_hogar = filtrar_df(archivo,lugares_de_interes)
#asignar valor aleatoreo a la fecha dentro de un rango definido en el catalogo de fechas
datos_compras_hogar['fecha_adqu'] = datos_compras_hogar['fecha_adqu'].map( lambda x : random.randint(1801, 1812))
#remplazar la clave por su contenido en el catalogo de fechas
datos_compras_hogar['fecha_adqu'] = datos_compras_hogar['fecha_adqu'].map( catalogo_fecha.set_index('fecha_adqu')['descripcion'])
#datos_compras_hogar['fecha_adqu2'] = datos_compras_hogar['fecha_adqu'].map( lambda x : formato_fecha(x))
datos_compras_hogar['fecha_adqu_form'] = datos_compras_hogar['fecha_adqu'].map( lambda x : formato_fecha(x))

#asignar valor aleatoreo a la frecuencia dentro de un rango definido en el catalogo de frecuencia
datos_compras_hogar['frecuencia'] = datos_compras_hogar['frecuencia'].map( lambda x : random.randint(0, 6))

datos_procesados = datos_compras_hogar

#conteo de ventas por lugar de interes, agrupacion por lugar de interes
datos_compras_hogar = datos_compras_hogar.groupby(["lugar_comp","clave"])["clave"].count().reset_index(name='total')
datos_compras_hogar = datos_compras_hogar.groupby(["lugar_comp"],sort=True)

#calculo de las ventas totales por lugar de interes
datos_compras_hogar2 = datos_compras_hogar
datos_compras_hogar2 = datos_compras_hogar2['total'].sum().reset_index(name='total')

#imprimir resultado de productos mas vendidos por lugar de interes
for i in lugares_de_interes:
    print("-+"*35)
    #obtener lugar de compra del catalogo
    print(f"Productos más vendido en: {(catalogo_lugar.set_index('lugar_comp')['descripcion'])[i]}")
    print("-+"*35)
    #seleccionar columnas a imprimir
    resultado = datos_compras_hogar.get_group(i)[['clave','total']]
    #ordenar claves por mayor ocurrencia
    resultado = resultado.sort_values(by=['total'], ascending=False)
    #obtener productos del catalogo
    resultado['producto'] = resultado['clave'].map(catalogo_productos.set_index('gastos')['descripcion'])
    #imprimir top 10 de productos por lugar de compra
    print(resultado.head(10).to_string(index=False), end='\n\n')


resultado2 = datos_compras_hogar2
resultado2 = resultado2.sort_values(by=['total'], ascending=False)

#imprimir resultado de lugar de interes que vende mas productos
for i in resultado2.index:
    resultado2['lugar_comp'][i] = (catalogo_lugar.set_index('lugar_comp')['descripcion'])[resultado2['lugar_comp'][i]]

print("-+"*35)
print("Total de ventas por lugar de interes")
print("-+"*35)
print(resultado2.head(10).to_string(index=False), end='\n\n')

#adicional
#obtener ventas por mes de cada lugar de interes
for i in lugares_de_interes:
    print("-+"*35)
    #obtener lugar de compra del catalogo
    print(f"Ventas totales por mes en: {(catalogo_lugar.set_index('lugar_comp')['descripcion'])[i]}")
    print("-+"*35)
    resultado3 = datos_procesados[datos_procesados.lugar_comp == i].groupby(["fecha_adqu","clave"])["clave"].count().reset_index(name='total')
    resultado3 = resultado3.groupby(['fecha_adqu'])
    resultado3 = resultado3['total'].sum().reset_index(name='total').sort_values(by=['total'], ascending=False)
    print(resultado3.head(10).to_string(index=False), end='\n\n')

#exportar a csv
#reiniciar indices para usar como llave
datos_procesados.reset_index(inplace=True)
archivo_exp = datos_procesados[["tipo_gasto", "clave", "forma_pag1", "lugar_comp", "frecuencia", "fecha_adqu_form"]]
archivo_exp = archivo_exp.rename(columns={"forma_pag1":"forma_pag", "fecha_adqu_form":"fecha_adqu"})
#print(archivo_exp.head())
archivo_exp.to_csv('out.csv',index=True)